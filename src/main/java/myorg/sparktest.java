package myorg;


import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.rabbitmq.RabbitMQUtils;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class sparktest {
    public static void main(String[] args) throws InterruptedException, SQLException {

        java.sql.Connection conn = DriverManager.getConnection("jdbc:mariadb://192.168.99.100:3306/db?user=user1&password=secret");
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("DROP TABLE IF EXISTS FEHLER");
        stmt.executeUpdate("CREATE TABLE FEHLER (id INTEGER AUTO_INCREMENT, fehlerid INTEGER, diff CHAR(100), datenid INTEGER, PRIMARY KEY ( id ));");
        stmt.close();
        conn.close();

        SparkConf sparkConf = new SparkConf().setAppName("JavaRabbitMQConsumer").setMaster("local[1]"); //"local[4]" to run locally with 4 cores
        System.setProperty("hadoop.home.dir", "C:/winutils");
        JavaStreamingContext jsc = new JavaStreamingContext(sparkConf, Durations.milliseconds(1));
        Map<String, String> params = new HashMap<String, String>();

        params.put("hosts", "192.168.99.100");
        params.put("queueName", "csv2rabbit");
        params.put("vHost", "/");
        params.put("userName", "guest");
        params.put("password", "guest");

        AtomicReference<Integer> i = new AtomicReference<>(0);
        final String[][] alt = {null};
        AtomicReference<Integer> nummer = new AtomicReference<>(0);
        AtomicReference<Integer> nummerAlt = new AtomicReference<>(0);
        AtomicReference<Integer> nummerAlt1 = new AtomicReference<>(0);
        AtomicReference<Integer> nummerAlt2 = new AtomicReference<>(0);
        AtomicReference<Integer> idFehlerDrei = new AtomicReference<Integer>(0);
        AtomicReference<Integer> idFehlerDrei2 = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisDrei = new AtomicReference<>(true);
        AtomicReference<Integer> idFehlerFunf = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisFunf = new AtomicReference<>(true);
        AtomicReference<Integer> idFehlerSechs = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisSechs = new AtomicReference<>(true);
        AtomicReference<Integer> idFehlerSieben = new AtomicReference<Integer>(0);
        AtomicReference<Integer> idFehlerSieben2 = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisSieben = new AtomicReference<>(true);

        JavaReceiverInputDStream<String> dStream =
                RabbitMQUtils.createJavaStream(jsc, String.class, params, message -> {
                    String msg = new String(message.getBody());
                    java.util.Date dt = new java.util.Date();
                    java.text.SimpleDateFormat sdfmmilsec = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    String ankunft = sdfmmilsec.format(dt);
                    nummer.getAndSet(nummer.get() + 1);
                    System.out.println(msg +";"+ ankunft);
                    String[] neu = msg.split(";");
                    Integer Ids = Integer.valueOf(neu[0]);
                    String Date = String.valueOf(neu[117]);
                    Integer Sensor10025 = Integer.valueOf(neu[26]);
                    Integer Sensor10026 = Integer.valueOf(neu[27]);
                    Integer Sensor10027 = Integer.valueOf(neu[28]);
                    Integer Sensor10028 = Integer.valueOf(neu[29]);
                    Integer Sensor10029 = Integer.valueOf(neu[30]);
                    Integer Sensor10030 = Integer.valueOf(neu[31]);
                    Integer Sensor10031 = Integer.valueOf(neu[32]);
                    Integer Sensor10039 = Integer.valueOf(neu[40]);
                    Integer Sensor10040 = Integer.valueOf(neu[41]);
                    Integer Sensor10048 = Integer.valueOf(neu[49]);
                    Integer Sensor20028 = Integer.valueOf(neu[88]);
                    Integer Sensor20030 = Integer.valueOf(neu[90]);
                    Integer Sensor20033 = Integer.valueOf(neu[93]);
                    Integer Sensor20034 = Integer.valueOf(neu[94]);
                    Integer Sensor20035 = Integer.valueOf(neu[95]);
                    Integer Sensor20037 = Integer.valueOf(neu[97]);
                    Integer Sensor20038 = Integer.valueOf(neu[98]);
                    Integer Sensor20042 = Integer.valueOf(neu[102]);
                    Integer Sensor20044 = Integer.valueOf(neu[104]);
                    Integer Sensor20046 = Integer.valueOf(neu[106]);
                    Integer Sensor20050 = Integer.valueOf(neu[110]);
                    Integer Sensor20051 = Integer.valueOf(neu[111]);
                    Integer Sensor20053 = Integer.valueOf(neu[113]);
                    Integer Sensor20054 = Integer.valueOf(neu[114]);
                    Integer Sensor20055 = Integer.valueOf(neu[115]);
                    String Stetig = String.valueOf(neu[116]);
                    Integer altSensor10039 = 0;
                    Integer altSensor10040 = 0;
                    Integer altSensor20037 = 0;
                    Integer altSensor20030 = 0;
                    Integer altSensor20033 = 0;
                    Integer altSensor20046 = 0;
                    Integer altSensor20053 = 0;
                    Integer altSensor20054 = 0;
                    Integer altSensor20055 = 0;
                    Integer altSensor20028 = 0;
                    Integer altSensor20038 = 0;
                    Integer altIds = 0;
                    if (i.get() > 0) {
                        altIds = Integer.valueOf(alt[0][0]);
                        String altDate = String.valueOf(alt[0][117]);
                        Integer altSensor10025 = Integer.valueOf(alt[0][26]);
                        Integer altSensor10026 = Integer.valueOf(alt[0][27]);
                        Integer altSensor10027 = Integer.valueOf(alt[0][28]);
                        Integer altSensor10028 = Integer.valueOf(alt[0][29]);
                        Integer altSensor10029 = Integer.valueOf(alt[0][30]);
                        Integer altSensor10030 = Integer.valueOf(alt[0][31]);
                        Integer altSensor10031 = Integer.valueOf(alt[0][32]);
                        altSensor10039 = Integer.valueOf(alt[0][40]);
                        altSensor10040 = Integer.valueOf(alt[0][41]);
                        Integer altSensor10048 = Integer.valueOf(alt[0][49]);
                        altSensor20028 = Integer.valueOf(alt[0][88]);
                        altSensor20030 = Integer.valueOf(alt[0][90]);
                        altSensor20033 = Integer.valueOf(alt[0][93]);
                        Integer altSensor20034 = Integer.valueOf(alt[0][94]);
                        Integer altSensor20035 = Integer.valueOf(alt[0][95]);
                        altSensor20037 = Integer.valueOf(alt[0][97]);
                        altSensor20038 = Integer.valueOf(alt[0][98]);
                        Integer altSensor20042 = Integer.valueOf(alt[0][102]);
                        Integer altSensor20044 = Integer.valueOf(alt[0][104]);
                        altSensor20046 = Integer.valueOf(alt[0][106]);
                        Integer altSensor20050 = Integer.valueOf(alt[0][110]);
                        Integer altSensor20051 = Integer.valueOf(alt[0][111]);
                        altSensor20053 = Integer.valueOf(alt[0][113]);
                        altSensor20054 = Integer.valueOf(alt[0][114]);
                        altSensor20055 = Integer.valueOf(alt[0][115]);
                        String altStetig = String.valueOf(alt[0][116]);
                    }
                    alt[0] = neu;
                    i.set(1);

                    if (Ids != altIds +1){
                        String ausgabe = "ID wurde übersprungen!!!! aktuelle ID: " + Ids + " alte Id: "+ altIds;
                        System.out.println(ausgabe);
                    }

                    // erster Fehlerfall
                    if (altSensor10039 == 0 && altSensor10040 == 0 && Sensor10039 == 1 && Sensor10040 == 1 || altSensor10039 == 1 && altSensor10040 == 0 && Sensor10039 == 1 && Sensor10040 == 1 || altSensor10039 == 0 && altSensor10040 == 1 && Sensor10039 == 1 && Sensor10040 == 1) {
                        try{
                            if ((nummer.get() - nummerAlt.get()) >= 20){
                                nummerAlt.getAndSet(nummer.get());
                                java.util.Date dat = new java.util.Date();
//                            String verarbeitet = sdfmmilsec.format(dat);
                                java.util.Date date1 = sdfmmilsec.parse(ankunft);
                                long diff = dat.getTime() - date1.getTime();
                                diff = (int) diff;
                                String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 1 +", " +diff +", "+ Ids+")";
                                SendResult.sendResult(ausgabe);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //zweiter Fehlerfall
                    if ((Sensor10025 == 1 || Sensor10026 == 1 || Sensor10027 == 1 || Sensor10028 == 1 || Sensor10029 == 1 || Sensor10030 == 1 || Sensor10031 == 1) && Sensor10048 == 1){
                        try{
                            if ((nummer.get() - nummerAlt1.get()) >= 20){
                                nummerAlt1.getAndSet(nummer.get());
                                java.util.Date dat = new java.util.Date();
//                            String verarbeitet = sdfmmilsec.format(dat);
                                Date date1 = sdfmmilsec.parse(ankunft);
                                long diff = dat.getTime() - date1.getTime();
                                diff = (int) diff;
                                String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 2 +", " +diff+", "+ Ids+ ")";
                                SendResult.sendResult(ausgabe);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //dritter Fehlerfall
                    if (Sensor20037 == 1 && altSensor20037 == 1){
                        idFehlerDrei.set(nummer.get());
                    }
                    if (Sensor20030 == 1 && altSensor20030 == 0 && (nummer.get() - idFehlerDrei.get()) <= 6){
                        idFehlerDrei2.set(nummer.get());
                        ergebnisDrei.set(false);
                    }
                    if (Sensor20033 ==1 && altSensor20033 ==0 && (nummer.get() - idFehlerDrei2.get()) <= 12) {
                        ergebnisDrei.set(true);
                    } else if ((nummer.get() - idFehlerDrei2.get()) >= 12 && !ergebnisDrei.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
                            //                    String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 3 +", " +diff +", "+ Ids +")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisDrei.set(true);
                    }

                    //vierter Fehlerfall
                    if (Sensor10025 == 0 && Sensor10031 == 1){
                        try{
                            if ((nummer.get() - nummerAlt2.get()) >= 20){
                                nummerAlt2.getAndSet(nummer.get());
                                java.util.Date dat = new java.util.Date();
//                            String verarbeitet = sdfmmilsec.format(dat);
                                Date date1 = sdfmmilsec.parse(ankunft);
                                long diff = dat.getTime() - date1.getTime();
                                diff = (int) diff;
                                String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 4 +", " +diff +", "+ Ids +")";
                                SendResult.sendResult(ausgabe);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //fünfter Fehlerfall
                    if (Sensor20046 == 1 && altSensor20046 == 0) {
                        idFehlerFunf.set(nummer.get());
                        ergebnisFunf.set(false);
                    }
                    if ((Sensor20053 ==1 && altSensor20053 ==0 || Sensor20054 ==1 && altSensor20054 ==0 || Sensor20055 ==1 && altSensor20055 ==0) && (nummer.get() - idFehlerFunf.get()) <= 18) {
                        ergebnisFunf.set(true);
                    } else if ((nummer.get() - idFehlerFunf.get()) >= 18 && !ergebnisFunf.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
//                        String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 5 +", " +diff +", "+ Ids+")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisFunf.set(true);
                    }

                    //sechster Fehlerfall
                    if (Sensor10030 ==1 && Sensor10025 == 1){
                        idFehlerSechs.set(nummer.get());
                        ergebnisSechs.set(false);
                    }
                    if (Sensor20028 ==1 && altSensor20028 ==0 && (nummer.get() - idFehlerSechs.get()) <= 12) {
                        ergebnisSechs.set(true);
                    } else if ((nummer.get() - idFehlerSechs.get()) >= 12 && !ergebnisSechs.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
                            //                    String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 6 +", " +diff +", "+ Ids +")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisSechs.set(true);
                    }

                    //siebter Fehlerfall
                    if (Sensor20038 ==1 && altSensor20038 == 0){
                        idFehlerSieben.set(nummer.get());
                    }
                    if (Sensor20038 ==0 && altSensor20038 == 1 && (nummer.get() - idFehlerSieben.get()) <= 25){
                        idFehlerSieben2.set(nummer.get());
                        ergebnisSieben.set(false);
                    }
                    if (Sensor20035 ==1 && Sensor20042 ==1 && (nummer.get() - idFehlerSieben2.get()) <= 6) {
                        ergebnisSieben.set(true);
                    } else if ((nummer.get() - idFehlerSieben2.get()) >= 6 && !ergebnisSieben.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
                            //                    String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 7 +", " +diff +", "+ Ids +")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisSieben.set(true);
                    }
                    return msg;
                });

        dStream.print();

        jsc.start();
        jsc.awaitTermination();
    }
}
