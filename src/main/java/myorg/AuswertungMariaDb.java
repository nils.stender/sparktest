package myorg;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static java.lang.Math.sqrt;

public class AuswertungMariaDb {
    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mariadb://192.168.99.100:3306/db?user=user1&password=secret");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select avg(diff) from FEHLER");
            if(rs.next())
                System.out.println("avg diff: " + rs.getFloat(1));

            ResultSet rs2 = stmt.executeQuery("select VARIANCE(diff) from FEHLER");
            if(rs2.next())
                System.out.println("varianz: " + rs2.getFloat(1));
                System.out.println("Standard: " + sqrt(rs2.getFloat(1)));

            ResultSet rs3 = stmt.executeQuery("SELECT COUNT(*) FROM FEHLER");
            if(rs3.next())
                System.out.println("Anzahl: " + rs3.getFloat(1));

//            ResultSet rs4 = stmt.executeQuery("SELECT datenid, avg(diff) FROM KAFKASENSOR200 GROUP BY datenid ORDER BY datenid");
//            if(rs4.next())
//                System.out.println("Bildspezifisch: " + rs4.getString(3));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
