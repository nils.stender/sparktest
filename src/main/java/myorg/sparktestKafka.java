package myorg;


import kafka.serializer.StringDecoder;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

public class sparktestKafka {

    public static SensorEvent SE = new SensorEvent();
    public static SensorEventAlt SEA = new SensorEventAlt();

    public static void main(String[] args) throws InterruptedException, SQLException {
        java.sql.Connection conn = DriverManager.getConnection("jdbc:mariadb://192.168.99.100:3306/db?user=user1&password=secret");
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("DROP TABLE IF EXISTS FEHLER");
        stmt.executeUpdate("CREATE TABLE FEHLER (id INTEGER AUTO_INCREMENT, fehlerid INTEGER, diff CHAR(100), datenid INTEGER, PRIMARY KEY ( id ));");
        stmt.close();
        conn.close();

        SparkConf sc = new SparkConf().setAppName("Kafka-Spark").setMaster("local[1]");

        AtomicReference<Integer> nummer = new AtomicReference<>(0);
        AtomicReference<Integer> nummerAlt = new AtomicReference<>(0);
        AtomicReference<Integer> nummerAlt1 = new AtomicReference<>(0);
        AtomicReference<Integer> nummerAlt2 = new AtomicReference<>(0);
        AtomicReference<Integer> idFehlerDrei = new AtomicReference<Integer>(0);
        AtomicReference<Integer> idFehlerDrei2 = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisDrei = new AtomicReference<>(true);
        AtomicReference<Integer> idFehlerFunf = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisFunf = new AtomicReference<>(true);
        AtomicReference<Integer> idFehlerSechs = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisSechs = new AtomicReference<>(true);
        AtomicReference<Integer> idFehlerSieben = new AtomicReference<Integer>(0);
        AtomicReference<Integer> idFehlerSieben2 = new AtomicReference<Integer>(0);
        AtomicReference<Boolean> ergebnisSieben = new AtomicReference<>(true);
        SE.setAll("0;2018-11-14 12:04:26.185;1;0;0;1;0;0;0;0;1;0;0;1;1;0;0;0;0;1;0;1;1;1;1;0;0;0;0;0;0;0;0;0;0;1;1;0;0;1;0;1;1;1;0;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;1;0;0;1;0;0;0;0;0;1;0;0;1;1;1;0;1;0;1;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;1;1;0;0;0;0;0;1294.85;2019-10-13 00:55:17.216");

        try(JavaStreamingContext jsc = new JavaStreamingContext(sc, new Duration(1))) {

            JavaPairInputDStream<String, String> stream = KafkaUtils.createDirectStream(
                    jsc, String.class, String.class, StringDecoder.class, StringDecoder.class,
                    Collections.singletonMap("metadata.broker.list", "132.187.226.20:9092"),
                    Collections.singleton("kafka1"));


            stream.foreachRDD(rdd -> {
                rdd.foreach(message -> {
                    String msg = message._2;
                    java.util.Date dt = new java.util.Date();
                    java.text.SimpleDateFormat sdfmmilsec = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    String ankunft = sdfmmilsec.format(dt);
                    nummer.getAndSet(nummer.get() + 1);
                    System.out.println(msg +";"+ ankunft);
                    String[] neu = msg.split(";");
                    SE.setDate2(ankunft);
                    SE.setIds(Integer.valueOf(neu[0]));
                    SE.setSensor10025(Integer.valueOf(neu[26]));
                    SE.setSensor10026(Integer.valueOf(neu[27]));
                    SE.setSensor10027(Integer.valueOf(neu[28]));
                    SE.setSensor10028(Integer.valueOf(neu[29]));
                    SE.setSensor10029(Integer.valueOf(neu[30]));
                    SE.setSensor10030(Integer.valueOf(neu[31]));
                    SE.setSensor10031(Integer.valueOf(neu[32]));
                    SE.setSensor10039(Integer.valueOf(neu[40]));
                    SE.setSensor10040(Integer.valueOf(neu[41]));
                    SE.setSensor10048(Integer.valueOf(neu[49]));
                    SE.setSensor20028(Integer.valueOf(neu[88]));
                    SE.setSensor20030(Integer.valueOf(neu[90]));
                    SE.setSensor20033(Integer.valueOf(neu[93]));
                    SE.setSensor20035(Integer.valueOf(neu[95]));
                    SE.setSensor20037(Integer.valueOf(neu[97]));
                    SE.setSensor20038(Integer.valueOf(neu[98]));
                    SE.setSensor20042(Integer.valueOf(neu[102]));
                    SE.setSensor20046(Integer.valueOf(neu[106]));
                    SE.setSensor20053(Integer.valueOf(neu[113]));
                    SE.setSensor20054(Integer.valueOf(neu[114]));
                    SE.setSensor20055(Integer.valueOf(neu[115]));
                    String[] alt = SE.getAll().split(";");

                    SEA.setIds(Integer.valueOf(alt[0]));
                    SEA.setSensor10039(Integer.valueOf(alt[40]));
                    SEA.setSensor20028(Integer.valueOf(alt[88]));
                    SEA.setSensor20030(Integer.valueOf(alt[90]));
                    SEA.setSensor20033(Integer.valueOf(alt[93]));
                    SEA.setSensor20037(Integer.valueOf(alt[97]));
                    SEA.setSensor20038(Integer.valueOf(alt[98]));
                    SEA.setSensor20046(Integer.valueOf(alt[106]));
                    SEA.setSensor20053(Integer.valueOf(alt[113]));
                    SEA.setSensor20054(Integer.valueOf(alt[114]));
                    SEA.setSensor20055(Integer.valueOf(alt[115]));

                    SE.setAll(msg);

                    if (SE.getIds() != SEA.getIds() +1){
                        String ausgabe = "ID wurde übersprungen!!!! aktuelle ID: " + SE.getIds() + " alte Id: "+ SEA.getIds();
                        System.out.println(ausgabe);
                    }
                    // erster Fehlerfall
                    if (SEA.getSensor10039() == 0 && SEA.getSensor10040() == 0 && SE.getSensor10039() == 1 && SE.getSensor10040() == 1 || SEA.getSensor10039() == 1 && SEA.getSensor10040() == 0 && SE.getSensor10039() == 1 && SE.getSensor10040() == 1 || SEA.getSensor10039() == 0 && SEA.getSensor10040() == 1 && SE.getSensor10039() == 1 && SE.getSensor10040() == 1) {
                        try{
                            if ((nummer.get() - nummerAlt.get()) >= 20){
                                nummerAlt.getAndSet(nummer.get());
                                java.util.Date dat = new java.util.Date();
//                            String verarbeitet = sdfmmilsec.format(dat);
                                java.util.Date date1 = sdfmmilsec.parse(ankunft);
                                long diff = dat.getTime() - date1.getTime();
                                diff = (int) diff;
                                String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 1 +", " +diff +", "+ SE.getIds()+")";
                                SendResult.sendResult(ausgabe);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //zweiter Fehlerfall
                    if ((SE.getSensor10025() == 1 || SE.getSensor10026() == 1 || SE.getSensor10027() == 1 || SE.getSensor10028() == 1 || SE.getSensor10029() == 1 || SE.getSensor10030() == 1 || SE.getSensor10031() == 1) && SE.getSensor10048() == 1){
                        try{
                            if ((nummer.get() - nummerAlt1.get()) >= 20){
                                nummerAlt1.getAndSet(nummer.get());
                                java.util.Date dat = new java.util.Date();
//                            String verarbeitet = sdfmmilsec.format(dat);
                                java.util.Date date1 = sdfmmilsec.parse(ankunft);
                                long diff = dat.getTime() - date1.getTime();
                                diff = (int) diff;
                                String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 2 +", " +diff+", "+ SE.getIds()+ ")";
                                SendResult.sendResult(ausgabe);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //dritter Fehlerfall
                    if (SE.getSensor20037() == 1 && SEA.getSensor20037() == 1){
                        idFehlerDrei.set(nummer.get());
                    }
                    if (SE.getSensor20030() == 1 && SEA.getSensor20030() == 0 && (nummer.get() - idFehlerDrei.get()) <= 6){
                        idFehlerDrei2.set(nummer.get());
                        ergebnisDrei.set(false);
                    }
                    if (SE.getSensor20033() ==1 && SEA.getSensor20033() ==0 && (nummer.get() - idFehlerDrei2.get()) <= 12) {
                        ergebnisDrei.set(true);
                    } else if ((nummer.get() - idFehlerDrei2.get()) >= 12 && !ergebnisDrei.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
                            //                    String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 3 +", " +diff +", "+ SE.getIds() +")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisDrei.set(true);
                    }

                    //vierter Fehlerfall
                    if (SE.getSensor10025() == 0 && SE.getSensor10031() == 1){
                        try{
                            if ((nummer.get() - nummerAlt2.get()) >= 20){
                                nummerAlt2.getAndSet(nummer.get());
                                java.util.Date dat = new java.util.Date();
//                            String verarbeitet = sdfmmilsec.format(dat);
                                Date date1 = sdfmmilsec.parse(ankunft);
                                long diff = dat.getTime() - date1.getTime();
                                diff = (int) diff;
                                String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 4 +", " +diff +", "+ SE.getIds() +")";
                                SendResult.sendResult(ausgabe);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //fünfter Fehlerfall
                    if (SE.getSensor20046() == 1 && SEA.getSensor20046() == 0) {
                        idFehlerFunf.set(nummer.get());
                        ergebnisFunf.set(false);
                    }
                    if ((SE.getSensor20053() ==1 && SEA.getSensor20053() ==0 || SE.getSensor20054() ==1 && SEA.getSensor20054() ==0 || SE.getSensor20055() ==1 && SEA.getSensor20055() ==0) && (nummer.get() - idFehlerFunf.get()) <= 18) {
                        ergebnisFunf.set(true);
                    } else if ((nummer.get() - idFehlerFunf.get()) >= 18 && !ergebnisFunf.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
//                        String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 5 +", " +diff +", "+ SE.getIds()+")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisFunf.set(true);
                    }

                    //sechster Fehlerfall
                    if (SE.getSensor10030() ==1 && SE.getSensor10025() == 1){
                        idFehlerSechs.set(nummer.get());
                        ergebnisSechs.set(false);
                    }
                    if (SE.getSensor20028() ==1 && SEA.getSensor20028() ==0 && (nummer.get() - idFehlerSechs.get()) <= 12) {
                        ergebnisSechs.set(true);
                    } else if ((nummer.get() - idFehlerSechs.get()) >= 12 && !ergebnisSechs.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
                            //                    String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 6 +", " +diff +", "+ SE.getIds() +")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisSechs.set(true);
                    }

                    //siebter Fehlerfall
                    if (SE.getSensor20038() ==1 && SEA.getSensor20038() == 0){
                        idFehlerSieben.set(nummer.get());
                    }
                    if (SE.getSensor20038() ==0 && SEA.getSensor20038() == 1 && (nummer.get() - idFehlerSieben.get()) <= 25){
                        idFehlerSieben2.set(nummer.get());
                        ergebnisSieben.set(false);
                    }
                    if (SE.getSensor20035() ==1 && SE.getSensor20042() ==1 && (nummer.get() - idFehlerSieben2.get()) <= 6) {
                        ergebnisSieben.set(true);
                    } else if ((nummer.get() - idFehlerSieben2.get()) >= 6 && !ergebnisSieben.get()){
                        try {
                            java.util.Date dat = new java.util.Date();
                            //                    String verarbeitet = sdfmmilsec.format(dat);
                            Date date1 = sdfmmilsec.parse(ankunft);
                            long diff = dat.getTime() - date1.getTime();
                            diff = (int) diff;
                            String ausgabe = "INSERT INTO FEHLER VALUES (Null, " + 7 +", " +diff +", "+ SE.getIds() +")";
                            SendResult.sendResult(ausgabe);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ergebnisSieben.set(true);
                    }
                });
            });

            jsc.start();
            jsc.awaitTermination();
        }
    }
}
