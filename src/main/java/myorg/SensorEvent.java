package myorg;

public class SensorEvent {
    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public int getSensor10009() {
        return sensor10009;
    }

    public void setSensor10009(int sensor10009) {
        this.sensor10009 = sensor10009;
    }

    public int getSensor10019() {
        return sensor10019;
    }

    public void setSensor10019(int sensor10019) {
        this.sensor10019 = sensor10019;
    }

    public int getSensor10025() {
        return sensor10025;
    }

    public void setSensor10025(int sensor10025) {
        this.sensor10025 = sensor10025;
    }

    public int getSensor10026() {
        return sensor10026;
    }

    public void setSensor10026(int sensor10026) {
        this.sensor10026 = sensor10026;
    }

    public int getSensor10027() { return sensor10027; }

    public void setSensor10027(int sensor10027) {
        this.sensor10027 = sensor10027;
    }

    public int getSensor10028() {
        return sensor10028;
    }

    public void setSensor10028(int sensor10028) {
        this.sensor10028 = sensor10028;
    }

    public int getSensor10029() {
        return sensor10029;
    }

    public void setSensor10029(int sensor10029) {
        this.sensor10029 = sensor10029;
    }

    public int getSensor10030() {
        return sensor10030;
    }

    public void setSensor10030(int sensor10030) {
        this.sensor10030 = sensor10030;
    }

    public int getSensor10031() {
        return sensor10031;
    }

    public void setSensor10031(int sensor10031) {
        this.sensor10031 = sensor10031;
    }

    public int getSensor10039() {
        return sensor10039;
    }

    public void setSensor10039(int sensor10039) {
        this.sensor10039 = sensor10039;
    }

    public int getSensor10040() {
        return sensor10040;
    }

    public void setSensor10040(int sensor10040) {
        this.sensor10040 = sensor10040;
    }

    public int getSensor10048() {
        return sensor10048;
    }

    public void setSensor10048(int sensor10048) {
        this.sensor10048 = sensor10048;
    }

    public int getSensor20028() {
        return sensor20028;
    }

    public void setSensor20028(int sensor20028) {
        this.sensor20028 = sensor20028;
    }

    public int getSensor20030() {return sensor20030; }

    public void setSensor20030(int sensor20030) { this.sensor20030 = sensor20030; }

    public int getSensor20034() {return sensor20034; }

    public void setSensor20034(int sensor20034) { this.sensor20034 = sensor20034; }

    public int getSensor20035() {
        return sensor20035;
    }

    public void setSensor20035(int sensor20035) {
        this.sensor20035 = sensor20035;
    }

    public int getSensor20033() {
        return sensor20033;
    }

    public void setSensor20033(int sensor20033) {
        this.sensor20033 = sensor20033;
    }

    public int getSensor20037() {
        return sensor20037;
    }

    public void setSensor20037(int sensor20037) {
        this.sensor20037 = sensor20037;
    }

    public int getSensor20038() {return sensor20038; }

    public void setSensor20038(int sensor20038) { this.sensor20038 = sensor20038; }

    public int getSensor20042() {return sensor20042; }

    public void setSensor20042(int sensor20042) { this.sensor20042 = sensor20042; }

    public int getSensor20046() {
        return sensor20046;
    }

    public void setSensor20046(int sensor20046) {
        this.sensor20046 = sensor20046;
    }

    public int getSensor20048() {
        return sensor20048;
    }

    public void setSensor20048(int sensor20048) {
        this.sensor20048 = sensor20048;
    }

    public int getSensor20049() {
        return sensor20049;
    }

    public void setSensor20049(int sensor20049) {
        this.sensor20049 = sensor20049;
    }

    public int getSensor20050() {
        return sensor20050;
    }

    public void setSensor20050(int sensor20050) {
        this.sensor20050 = sensor20050;
    }

    public int getSensor20051() {
        return sensor20051;
    }

    public void setSensor20051(int sensor20051) {
        this.sensor20051 = sensor20051;
    }


    public int getSensor20053() {
        return sensor20053;
    }

    public void setSensor20053(int sensor20053) {
        this.sensor20053 = sensor20053;
    }

    public int getSensor20054() {
        return sensor20054;
    }

    public void setSensor20054(int sensor20054) {
        this.sensor20054 = sensor20054;
    }


    public int getSensor20055() {
        return sensor20055;
    }

    public void setSensor20055(int sensor20055) {
        this.sensor20055 = sensor20055;
    }


    public int getSensor20044() {
        return sensor20044;
    }

    public void setSensor20044(int sensor20044) {
        this.sensor20044 = sensor20044;
    }

    public String getSensorStetig() {
        return sensorStetig;
    }

    public void setStetig(String sensorStetig) {
        this.sensorStetig = sensorStetig;
    }



    private String date2;
    private String date;
    private String all;
    private int ids;
    private int sensor10009;
    private int sensor10019;
    private int sensor10025;
    private int sensor10026;
    private int sensor10027;
    private int sensor10028;
    private int sensor10029;
    private int sensor10030;
    private int sensor10031;
    private int sensor10039;
    private int sensor10040;
    private int sensor10048;
    private int sensor20030;
    private int sensor20033;
    private int sensor20028;
    private int sensor20034;
    private int sensor20035;
    private int sensor20038;
    private int sensor20042;
    private int sensor20037;
    private int sensor20044;
    private int sensor20046;
    private int sensor20048;
    private int sensor20049;
    private int sensor20050;
    private int sensor20051;
    private int sensor20053;
    private int sensor20054;
    private int sensor20055;
    private String sensorStetig;
}